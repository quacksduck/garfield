import time
import random as rand

__version__ = 1.1

def get(year, month, day):
    year = str(year).zfill(2)
    month = str(month).zfill(2)
    day = str(day).zfill(2)
    url = "https://d1ejxu6vysztl5.cloudfront.net/comics/garfield/{}/{}-{}-{}.gif".format(year, year, month, day)
    return url
def today():
    year = str(time.strftime("%Y"))
    month = str(time.strftime("%m"))
    day = str(time.strftime("%d"))
    url = "https://d1ejxu6vysztl5.cloudfront.net/comics/garfield/{}/{}-{}-{}.gif".format(year, year, month, day)
    return url
def yesterday():
    timestamp = int(time.time()) - 86400
    year = time.strftime("%Y", time.localtime(timestamp))
    month = time.strftime("%m", time.localtime(timestamp))
    day = time.strftime("%d", time.localtime(timestamp))
    url = "https://d1ejxu6vysztl5.cloudfront.net/comics/garfield/{}/{}-{}-{}.gif".format(year, year, month, day)
    return url
def random():
    timestamp = rand.randint(267112499, int(time.time()))
    year = time.strftime("%Y", time.localtime(timestamp))
    month = time.strftime("%m", time.localtime(timestamp))
    day = time.strftime("%d", time.localtime(timestamp))
    url = "https://d1ejxu6vysztl5.cloudfront.net/comics/garfield/{}/{}-{}-{}.gif".format(year, year, month, day)
    return url