from setuptools import setup
from garfield import __version__

setup(name='garfield',
      version=__version__,
      py_modules=['garfield'],
      description='Get garfield comics',
      platforms=['any']
      )